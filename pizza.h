#ifndef PIZZA_H
#define PIZZA_H
#include <string>
#include <iostream>

class Pizza
{
public:
    Pizza();
    void set_name(std::string name);
    void set_price(float price);
    std::string get_name() const;
    float get_price() const;
    friend std::ostream & operator << (std::ostream &out, const Pizza &c);
private:
    std::string name;
    float price;
};

#endif // PIZZA_H
