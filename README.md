## Pizzeria Italiana
Paweł Hajdo, Sebastian Mazgaj, Jacek Dusza
Projekt Narzędzia i środowiska programistyczne II - pizzeria

## O programie

Kod do programu został napisany w języku **C++** wraz z jego bibliotekami. Programiści korzystali z narzędzia deweloperskiego QT Creator.

Program ma na celu umożliwienie użytkownikowi złożenia zamówienia w restauracji.
Po otwarciu programu ukazuje się menu główne z logiem i krótkim powitaniem oraz możliwością wyboru zamówienia poszczególnych produktów. Wyświetlają się również dostępne promocje. Po złożeniu zamówienia użytkownik dostaje informacje o sfinalizowaniu zamówienia i wyświetleniu paragonu z zamówionymi produktami.

## Uruchamianie

Aby program działał prawidłowo wymagane są 3 pliki tekstowe, w których zawarta jest informacja o dostępnym menu restauracji.
```
pizzas.txt
drinks.txt
promotions.txt
```
## Funkcje

| Funkcja | Opis|
| ------ | ------ |
| start() | wywoływanie pozostałych funkcji |
| addToMenu() | wczytywanie menu restauracji z pliku tekstowego .txt |
| showLogo() | wyświetlenie logo na stronie głównej aplikacji |
| showMenu() | menu główne |
| showMenu_pizza(), showMenu_drink(), showMenu_promotions() | wyświetlenie menu restauracji z produktami |
| addToOrder_pizza(), addToOrder_drink() | dodawanie produktów do rachunku |
| promotions() | obsługa logiczna promocji |
| cancelOrder() | anulowanie zamówienia |
| generateBill() | tworzenie i wyświetlenie paragonu |

## Podział zadań

##### Paweł Hajdo:
- utworzenie repozytorium
- utworzeniu struktury plików
- stworzenie plików nagłówkowych z klasami
- funkcje set i get w klasach pizza/drink
- wyświetlanie menu, składanie zamówienia
- anulowanie zamówienia
- generowanie paragonu

##### Sebastian Mazgaj:
- dodanie napojów i wczytanie z pliku .txt
- obsługa promocji pizzy 50/50, darmowy napój, 20% taniej
- prostsze wyświetlanie produktów 
- poprawa błędów programu
- anulowanie zamówienia
- poprawa i ulepszenie składania zamówienia

##### Jacek Dusza:
- utworzenie plików tekstowych
- otwieranie i wczytywanie produktów z pliku tekstowego
- obsługa promocji pizzy 50/50, ceny
- obsługa wyjątków
- poprawienie błędów programu
- anulowanie zamówienia