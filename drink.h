#ifndef DRINK_H
#define DRINK_H
#include <string>
#include <iostream>

class Drink
{
public:
    Drink();
    void set_name(std::string name);
    void set_price(float price);
    std::string get_name() const;
    float get_price() const;
    friend std::ostream & operator << (std::ostream &out, const Drink &c);
private:
    std::string name;
    float price;
};

#endif // DRINK_H
