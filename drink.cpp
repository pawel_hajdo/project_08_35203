#include "drink.h"

Drink::Drink()
{

}

void Drink::set_name(std::string name)
{
    this->name = name;
}

void Drink::set_price(float price)
{
    this->price = price;
}

std::string Drink::get_name() const
{
    return name;
}

float Drink::get_price() const
{
    return price;
}


std::ostream& operator<<(std::ostream& os, const Drink& dt)
{
    os << dt.get_name() << ' ' << dt.get_price();
    return os;
}
