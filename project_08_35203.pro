TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        drink.cpp \
        main.cpp \
        order.cpp \
        pizza.cpp

HEADERS += \
    drink.h \
    order.h \
    pizza.h
