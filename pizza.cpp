#include "pizza.h"

Pizza::Pizza()
{

}

void Pizza::set_name(std::string name)
{
    this->name = name;
}

void Pizza::set_price(float price)
{
    this->price = price;
}

std::string Pizza::get_name() const
{
    return name;
}

float Pizza::get_price() const
{
    return price;
}

std::ostream& operator<<(std::ostream& os, const Pizza& dt)
{
    os << dt.get_name() << ' ';
    if(dt.get_price()>0)
    {
        os << dt.get_price();
    }

    return os;
}
