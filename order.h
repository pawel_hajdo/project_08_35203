#ifndef ORDER_H
#define ORDER_H
#include <iostream>
#include <vector>
#include "drink.h"
#include "pizza.h"
#include <fstream>
#include <string>
#include <cstdlib>
#include <iomanip>
#include <ctime>
#include <windows.h>

using namespace std;

class Order
{
public:
    Order();
    void start();
    void addToMenu();
    void showLogo();
    void showMenu();
    void showMenu_pizza();
    void showMenu_drink();
    void showMenu_promotions();
    void addToOrder_pizza();
    void addToOrder_drink();
    void promotions();
    void generateBill();
    void cancelOrder();
    unsigned int isNumber( unsigned int option );
    string name;
    float price;
    int quantity;
    vector<string>vPromotions;
private:
    vector<Drink>vDrinks;
    vector<Pizza>vPizzas;
    vector<Order>vOrder;   
};

#endif // ORDER_H
