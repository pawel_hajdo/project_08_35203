#include "order.h"

Order::Order()
{

}

int MENU=5;
int COUNTER = 0;

void Order::start()
{
    while(true)
    {
        switch(MENU)
        {
        case 5:
        {
            showLogo();
            showMenu();
            break;
        }
        case 1:
        {
            showMenu_pizza();
            addToOrder_pizza();
            break;
        }
        case 2:
        {
            showMenu_drink();
            addToOrder_drink();
            break;
        }
        case 3:
        {
            showMenu_promotions();
            break;
        }
        case 4:
        {
            cancelOrder();
            break;
        }
        case 0:
        {
            promotions();
            generateBill();
            return;
        }
        }
    }
}

unsigned int Order::isNumber( unsigned int option )
{
    while( cin.fail() )
    {
        cin.clear();
        cin.ignore(256,'\n');
        cout << "Podano niepoprawną wartość" << endl;
        cin >> option;
    }
    return option;
}

void Order::addToMenu()
{
    fstream file;
    file.open( "../project_08_35203/pizzas.txt", ios::in );

    if( file.good() != true )
    {
        cout << "Nie można otworzyć pliku" << endl;
        perror("Error: ");
        file.close();
        return;
    }

    while( file.eof() != true )
    {
        Pizza obj1;
        string name;
        file >> name;
        obj1.set_name(name);
        float price;
        file >> price;
        obj1.set_price(price);

        vPizzas.push_back(obj1);
    }
    file.close();
    file.clear();

    file.open( "../project_08_35203/drinks.txt", ios::in );

    if( file.good() != true )
    {
        cout << "Nie można otworzyć pliku" << endl;
        perror("Error");
        file.close();
        return;
    }

    while( file.eof() != true )
    {
        Drink obj1;
        string name;
        file >> name;
        obj1.set_name(name);
        float price;
        file >> price;
        obj1.set_price(price);

        vDrinks.push_back(obj1);
    }
    file.close();
    file.clear();

    file.open( "../project_08_35203/promotions.txt", ios::in );

    if( file.good() != true )
    {
        cout << "Nie można otworzyć pliku" << endl;
        perror("Error: ");
        file.close();
        return;
    }

    string promotion;
    while (file.eof() != true ){
        getline(file,promotion);
        vPromotions.push_back(promotion);
    }
    file.close();
    file.clear();
}

void Order::showLogo()
{
    system("cls");
    cout << "______ _                _____ _        _ _                   " << endl;
    cout << "| ___ (_)              |_   _| |      | (_)                  " << endl;
    cout << "| |_/ /_ __________ _    | | | |_ __ _| |_  __ _ _ __   __ _ " << endl;
    cout << "|  __/| |_  /_  / _` |   | | | __/ _` | | |/ _` | '_ \\ / _` |" << endl;
    cout << "| |   | |/ / / / (_| |  _| |_| || (_| | | | (_| | | | | (_| |" << endl;
    cout << "\\_|   |_/___/___\\__,_|  \\___/ \\__\\__,_|_|_|\\__,_|_| |_|\\__,_|\n" << endl;

    int logoWidth = 61;
    for(int i = 0 ; i < logoWidth ; i++ )
    {
        cout << "-";
    }
    cout << endl;
}

void Order::showMenu()
{
    cout << "Witamy w naszej pizzerii!" << endl;
    cout << "Możesz u nas zamówić prawdziwą włoską pizzę!" << endl;
    cout << "1. Zamów pizzę \n2. Zamów napój \n3. Wyświetl dostępne promocje \n4. Anuluj zamówienie \n0. Zakończ zamawianie i przejdź do kasy" << endl;

    unsigned int choice;
    cin >> choice;
    choice = isNumber( choice );
    switch (choice) {
    case 1:
    {
        MENU = 1;
        break;
    }
    case 2:
    {
        MENU = 2;
        break;
    }
    case 3:
    {
        MENU = 3;
        break;
    }
    case 4:
    {
        MENU = 4;
        break;
    }
    case 0:
    {
        MENU = 0;
        break;
    }
    default:
    {
        while( choice > 4 )
        {
            cout << "Podano niepoprawną wartość" << endl;
            cin >> choice;
            choice = isNumber( choice );
        }
        MENU = choice;
    }
    }
}

void Order::showMenu_pizza()
{
    int counter = 0;
    system("cls");
    cout << "Wybierz pizzę: \n" << endl;

    for(auto &value:vPizzas)
    {
        string tmp = value.get_name();
        unsigned long long end = 35-tmp.length();
        counter++;
        cout << counter << ". " << value.get_name();
        for(unsigned long long i = 0; i<end; i++)
        {
            cout << ".";
        }
        cout << setprecision(2) <<  fixed << value.get_price() << endl;

    }
    cout << "*cena pizzy 50/50 jest uśrednioną ceną dwóch połączonych pizz";
    cout << "\n\n9. Powrót do menu głównego" << endl;
    cout << "0. Zakończ zamawianie i wygeneruj paragon" << endl;
}

void Order::showMenu_drink()
{
    int counter = 0;
    system("cls");
    cout << "Wybierz napój: \n" << endl;
    for(auto &value:vDrinks)
    {
        string tmp = value.get_name();
        unsigned long long end = 35-tmp.length();
        counter++;
        cout << counter << ". " << value.get_name();
        for(unsigned long long i = 0; i<end; i++)
        {
            cout << ".";
        }
        cout << setprecision(2) <<  fixed << value.get_price() << endl;
    }
    cout << "\n9. Powrót do menu głównego" << endl;
    cout << "0. Zakończ zamawianie i wygeneruj paragon" << endl;
}

void Order::showMenu_promotions()
{
    int counter = 0;
    system("cls");
    cout << "Nasze promocje: \n" << endl;
    for(auto &value:vPromotions)
    {
        counter++;
        cout << counter << ". " << value << endl;
    }
    cout << "\n9. Powrót do menu głównego" << endl;
    cout << "0. Zakończ zamawianie i wygeneruj paragon" << endl;

    unsigned int option;
    cin >> option;
    option = isNumber( option );
    while( option != 9 && option != 0 )
    {
        cout << "Podano niepoprawną wartość" << endl;
        cin  >> option;
        option = isNumber( option );
    }
    if(option == 9)
    {
        MENU = 5;
        return;
    }
    if(option == 0)
    {
        MENU = 0;
        return;
    }
}

void Order::addToOrder_pizza()
{
    unsigned int option;
    unsigned int menuSize = vPizzas.size();

    do
    {
        Order obj;
        cin >> option;
        option = isNumber( option );
        if(option == 9)
        {
            MENU = 5;
            return;
        }
        else if(option == 0)
        {
            MENU = 0;
            return;
        }
        else if( option > vPizzas.size() )
        {
            cout << "Podano niepoprawna wartość" << endl;
            continue;
        }
        if( option == menuSize )
        {
            unsigned int temp;
            float price = 0;
            obj.name = vPizzas[menuSize-1].get_name();
            obj.quantity = 1;
            cout << "Wybierz pierwszą połowę do pizzy 50/50" << endl;
            cin  >> option;
            option = isNumber( option );
            while( option >= vPizzas.size() || option == 0)
            {
                cout << "Podano niepoprawną wartość" << endl;
                cin >> option;
                option = isNumber( option );
            }
            temp = option;
            cout << "Wybierz drugą połowę do pizzy 50/50" << endl;
            cin  >> option;
            option = isNumber( option );
            while( option >= vPizzas.size() || option == 0)
            {
                cout << "Podano niepoprawną wartość" << endl;
                cin >> option;
                option = isNumber( option );
            }
            price = (vPizzas[temp-1].get_price() + vPizzas[option-1].get_price()) / 2.0;
            obj.price = price;
            if( option == temp )
            {
                cout << "Hmm... wygląda na to, że wybrałeś dwie takie same połówki, połączyliśmy je zatem w jedną :)" << endl;
                obj.name = vPizzas[option-1].get_name();
                obj.price = vPizzas[option-1].get_price();
                obj.quantity = 1;
                cout << "Dodano " << vPizzas[option-1].get_name() << " do zamówienia" << endl;
            }
            else
            {
                cout << "Dodano " << vPizzas[temp-1].get_name() << "-" << vPizzas[option-1].get_name() << " pizza 50/50" << endl;
            }

            vOrder.push_back(obj);
            COUNTER++;
        }
        else
        {
            obj.name = vPizzas[option-1].get_name();
            obj.price = vPizzas[option-1].get_price();
            obj.quantity = 1;
            cout << "Dodano " << vPizzas[option-1].get_name() << " do zamówienia" << endl;
            bool flag = 1;
            for(auto &value:vOrder)
            {
                if(obj.name==value.name)
                {
                    value.quantity ++;
                    COUNTER++;
                    flag = 0;
                    break;
                }
            }
            if(flag)
            {
                vOrder.push_back(obj);
                COUNTER++;
            }
        }
    }while(option!=0 && option !=9);
}

void Order::addToOrder_drink()
{
    unsigned int option;
    Order obj;

    do
    {
        cin >> option;
        option = isNumber(option);
        if(option == 9)
        {
            MENU = 5;
            return;
        }
        else if(option == 0)
        {
            MENU = 0;
            return;
        }
        else if( option > vDrinks.size() )
        {
            cout << "Podano niepoprawną wartość" << endl;
            continue;
        }
        obj.name = vDrinks[option-1].get_name();
        obj.price = vDrinks[option-1].get_price();
        obj.quantity = 1;
        cout << "Dodano " << vDrinks[option-1].get_name() << " do zamówienia" << endl;
        bool flag = 1;
        for(auto &value:vOrder)
        {
            if(obj.name==value.name)
            {
                value.quantity ++;
                flag = 0;
                break;
            }
        }
        if(flag)
        {
            vOrder.push_back(obj);
        }
    }while(true);
}

void Order::promotions()
{
    if( COUNTER < 2 )
    {
        return;
    }
    int counter = 0;
    system("cls");
    if( COUNTER / 2 == 1 ){
        cout << "Dodatkowo do zamówienia napój gratis: " << endl;
    }
    else if( COUNTER / 2 < 5 ){
        cout << "Dodatkowo do zamówienia " << COUNTER / 2 << " napoje gratis: " << endl;
    } else {
        cout << "Dodatkowo do zamówienia " << COUNTER / 2 << " napojów gratis: " << endl;
    }

    cout << "Nasze napoje: " << endl;
    for(auto &value:vDrinks)
    {
        counter++;
        cout << counter << ". " << value.get_name() << endl;
    }

    unsigned int option;
    Order obj;

    for( int i = 0 ; i < COUNTER / 2 ; i++ )
    {
        cin  >> option;
        option = isNumber(option);
        if( option > vDrinks.size() )
        {
            cout << "Podano niepoprawną wartość" << endl;
            i--;
            continue;
        }
        obj.name = vDrinks[option-1].get_name();
        obj.price = 0.00;
        obj.quantity = 1;
        bool flag = 1;
        for(auto &value:vOrder)
        {
            if(obj.name==value.name && obj.price==value.price)
            {
                value.quantity ++;
                flag = 0;
                break;
            }
        }
        if(flag)
        {
            cout << "Dodano " << vDrinks[option-1].get_name() << " do zamówienia" << endl;
            vOrder.push_back(obj);
        }
    }
}

void Order::cancelOrder()
{
    system("cls");
    cout << "Twoje zamówienie" << endl;

    for(auto &value:vOrder)
    {
        string tmp =value.name;
        cout << value.name << " " << value.quantity << " x " << value.price;
        string tmp2 = to_string(value.price);
        unsigned long long end = 29-tmp.length() - tmp2.length();
        for(unsigned long long i = 0; i<end; i++)
        {
            cout << " ";
        }
        cout << setprecision(2) <<  fixed << value.price*value.quantity << endl;
    }
    char cancel;
    cout << "Czy na pewno chcesz anulować zamówienie? [Y/N]" << endl;
    cancel = getchar();

    if(cancel == 'Y' || cancel == 'y')
    {
        vOrder.clear();
        COUNTER = 0;
        cout << "Twoje zamówienie zostało anulowane" << endl;
        Sleep(2000);
        MENU = 5;
    }
    else if(cancel == 'N' || cancel == 'n')
    {
        cout << "Powrót do menu głównego" << endl;
        Sleep(2000);
        MENU = 5;
    }
}

void Order::generateBill()
{
    time_t now = time(0);

    char* dt = ctime(&now);
    float sum = 0;
    system("cls");
    cout << "\tPIZZERIA ITALIANA\n\t  Mickiewicza 8\n\t  33-100 Tarnów" << endl;
    for(int i = 0; i<35; i++)
    {
        cout << "-";
    }
    cout << "\n\tPARAGON FISKALNY" << endl;
    cout << "    " << dt << endl;
    for(auto &value:vOrder)
    {
        sum += value.price*value.quantity;
        string tmp =value.name;
        cout << value.name << " " << value.quantity << " x " << value.price;
        string tmp2 = to_string(value.price);
        unsigned long long end = 29-tmp.length() - tmp2.length();
        for(unsigned long long i = 0; i<end; i++)
        {
            cout << " ";
        }
        cout << setprecision(2) <<  fixed << value.price*value.quantity << endl;
    }
    if( sum >= 100 )
    {
        sum *= 0.8;
    }
    for(int i = 0; i<35; i++)
    {
        cout << "-";
    }
    cout << setprecision(2) <<  fixed <<"\nSuma:\t\t\t PLN " << sum << endl;
}
